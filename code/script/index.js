$(document).ready(function () {
    $('#slider').bxSlider({'pager': false, 'auto': true});

    //limpiamos el localStorage para próximas búsquedas
    localStorage.clear();

    //listeners para el breadcrumb
	$('#busqueda').click(function() {
		storeSearch();
	});


	$('a.category').click(function() {
		searchCategory(this);
	});

	$('a.subcategory').click(function() {
		var cat = this.parentNode.getElementsByTagName('a')[0];
		searchSubCategory(cat, this);
		
	});
});


/*
	Función que almacena el string de búsqueda
*/
function storeSearch(){
	localStorage.setItem('category', $(".search")[0][0].value);
	localStorage.setItem('search', $(".search")[0][1].value);
}

function searchCategory(element){
	localStorage.setItem('category', element.innerHTML);
}

function searchSubCategory(elementC, elementSC){
	localStorage.setItem('category', elementC.innerHTML);
	localStorage.setItem('subcategory', elementSC.innerHTML.toLowerCase());
}

