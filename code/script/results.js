$(document).ready(function () {
	$(function() {

	//Crea el slider
	$('#sliderPrecio').slider({
		animate: "fast",
		values: [0,100000],
		min: 0,
		max: 100000,
		range: true,
		step: 1,
		slide: function( event, ui ) {
			$("#rango").val( ui.values[0] + "€ - " + ui.values[1] +'€' );
		},
		change : function() {
			updateResults();
		}
	});

	//Como el método para modificar los items por página no funciona
	//hay 2 paginaciones dependiendo la vista
	$('#largePagination').pagination({
		items: products.length,
		itemsOnPage: 5,
		cssStyle: 'light-theme',
		onPageClick: function(pageNumber) {
			localStorage.setItem('largePage', pageNumber);
			updateResults();
		}
	});

	$('#shortPagination').pagination({
		items: products.length,
		itemsOnPage: 16,
		cssStyle: 'light-theme',
		onPageClick: function(pageNumber) {
			localStorage.setItem('shortPage', pageNumber);
			updateResults();
		}
	});

	var filtros = false;

	$('#busqueda').click(function() {
		//limpiamos localStorage para que el breadcrumb sea correcto
		localStorage.clear();
		storeSearch();
	});

	searchResults();

	$('#ordenar').click(function () {
		this.classList.toggle('fa-sort-amount-desc');
		this.classList.toggle('fa-sort-amount-asc');

		var sortAsc = localStorage.getItem('sort');
		if (sortAsc == undefined || sortAsc == 'true')
			localStorage.setItem('sort', 'false');
		else
			localStorage.setItem('sort', 'true');
		updateResults();
	});

	// Los elementos ocupan el ancho de la lista
	$('#largeItems').click(function () {
		localStorage.setItem('view', 'true');
		updateResults();
		document.getElementById('shortItems').classList.remove('active');
		$('#shortPagination').css('display','none');
		$('#largePagination').css('display','flex');
		this.classList.add('active');
	});

	// Los elementos tienen el ancho fijo
	$('#shortItems').click(function () {
		localStorage.setItem('view', 'false');
		updateResults();
		document.getElementById('largeItems').classList.remove('active');
		$('#shortPagination').css('display','flex');
		$('#largePagination').css('display','none');
		this.classList.add('active');
	});

	//actualizar resultados según filtros
	$('.filtros input').on('change', function() {
		updateResults();
	});

	//Para acceder al id guardado: localStorage.getItem("idObjeto") -- Sólo hay 1 id guardado, si se guarda
	//otro id en "idObjeto" se escribe sobre el valor antiguo

	function searchResults() {
		//Generar los objetos relacionados con la búsqueda.
		var category, search, subCat;
		if (getUrlVars()['category']) {
			category = getUrlVars()['category'];
		}
		if (getUrlVars()['subcategory']) {
			subCat = getUrlVars()['subcategory'];
		}
		if (getUrlVars()['search']) {
			search = getUrlVars()['search'].toLowerCase();
		}

		var viewLarge = localStorage.getItem('view');
		var sortAsc = localStorage.getItem('sort');
		var results = [];

		//caso enlaces de categorias/subcategorias
		if ((category != undefined) && (subCat != undefined))
			results = generateResults1(category, subCat);
		//barra de búsqueda con texto
		else if ((category != undefined) && (search != undefined))
			results = generateResults2(category, search);
		//búsqueda sólo por categoría
		else if (category != undefined)
			results = generateResults3(category);

		if (results.length > 0){
			var orderedRes = ordenarResults(results, sortAsc);
			renderResults(orderedRes, viewLarge);
			imagenesCuadradas();
		}
		else
			renderError();
	}

	function setNResultados(n) {
		$('section.results div.resultados span#cantidad').html(n + (n > 1 ? ' resultados' : ' resultado'));
	}

	/*
	Esta función genera un mapa cuya clave es el nombre de las variables pasadas por GET y el valor es el contenido de las mismas
	*/
	function getUrlVars() {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			if (value != '#')
				vars[key] = value;
			});
		return vars;
	}

	//Para hacer el código más bonito, métodos para crear los items en la página de "results"
	/*<a class='itemLarge' href='item.html?id=X'>
		<div class='imagen'>
			<img src='path/img'>
			<p>
				<i class='fa fa-share-alt'></i>
				<i class='fa fa-star-o'></i>
				<i class='fa fa-exclamation'></i>
			</p>
		</div>
		<div class='description'>
			<h3> Nombre </h3>
			<p> description
			</p>
		</div>
	</a>
	*/
	function createItemLarge (id, path, desc, name, prize) {
		var results = $('.resultados #anuncios')[0];

		//Crear y dar atributos al contenedor general
		var divItem = document.createElement('a');
		divItem.setAttribute('class', 'itemLarge');
		divItem.setAttribute('href', 'item.html?id='+id);

			//Elementos internos de divItem
			//Crear y dar atributos a la seccion de la imagen
			var divImg = document.createElement('div');
			divImg.setAttribute('class', 'imagen');
				//Elementos internos del div
				var img = document.createElement('img');
				img.setAttribute('src', path);
				img.setAttribute('alt', 'imagen');

				var pSocial = document.createElement('p');
				pSocial.innerHTML = "<i class='fa fa-share-alt'></i><i class='fa fa-star-o'></i><i class='fa fa-exclamation'></i>";

			divImg.appendChild(img);
			divImg.appendChild(pSocial);

			//Crear y dar atributos a la sección de descripción
			var divDesc = document.createElement('div');
			divDesc.setAttribute('class', 'description');
				//Elementos internos de divDesc

				var divHead = document.createElement('div');
				divHead.setAttribute('class', 'head');
				//Elementos internos del divHead
					var hName = document.createElement('h3');
					hName.innerHTML = name;

					var pDesc = document.createElement('p');
					pDesc.innerHTML = desc;

				divHead.appendChild(hName);
				divHead.appendChild(pDesc);

				var pPrize = document.createElement('p');
				pPrize.setAttribute('class', 'precio');
					var pPrizeTag = document.createElement('span');
					pPrizeTag.setAttribute('class', 'prizeTag')
					pPrizeTag.innerHTML = 'Precio:';

					var pPrizeVal = document.createElement('span');
					pPrizeVal.innerHTML = prize + '€';

				pPrize.appendChild(pPrizeTag);
				pPrize.appendChild(pPrizeVal);

			divDesc.appendChild(divHead);
			divDesc.appendChild(pPrize);

		divItem.appendChild(divImg);
		divItem.appendChild(divDesc);

		//una vez tenemos todo el elemento, lo agregamos a la lista de resultados
		results.appendChild(divItem);
	}

	/*
	<div class='itemShort'>
		<div class='imagen' href='item.html?id=X'>
			<img src='path'>
			<p>
				<i class='fa fa-share-alt'></i>
				<i class='fa fa-star-o'></i>
				<i class='fa fa-exclamation'></i>
			</p>
		</div>
	</div>
	*/
	function createItemShort (id, path,	prize) {
		var results = $('.resultados #anuncios')[0];

		//Crear y dar atributos al contenedor general
		var divItem = document.createElement('a');
		divItem.setAttribute('class', 'itemShort');
		divItem.setAttribute('href', 'item.html?id='+id);


			//Elementos internos de divItem
			//Crear y dar atributos a la seccion de la imagen
			var divImg = document.createElement('div');
			divImg.setAttribute('class', 'imagen');
				//Elementos internos del div
				var img = document.createElement('img');
				img.setAttribute('src', path);
				img.setAttribute('alt', 'imagen');

				var divBottom = document.createElement('div');
				divBottom.setAttribute('class', 'bottom');
				//Elementos internos del divBottom
					var pSocial = document.createElement('p');
					pSocial.innerHTML = "<i class='fa fa-share-alt'></i><i class='fa fa-star-o'></i><i class='fa fa-exclamation'></i>";

					var pPrize = document.createElement('p');
					pPrize.innerHTML = prize + '€';
					pPrize.setAttribute('class', 'precio')

					divBottom.appendChild(pSocial);
					divBottom.appendChild(pPrize);


			divImg.appendChild(img);
			divImg.appendChild(divBottom);

		divItem.appendChild(divImg);

		//una vez tenemos todo el elemento, lo agregamos a la lista de resultados
		results.appendChild(divItem);
	}

	/*
		Función que almacena el string de búsqueda
	*/
	function storeSearch(){
		localStorage.setItem('category', $(".search")[0][0].value);
		localStorage.setItem('search', $(".search")[0][1].value);
	}

	/*
	    Renderiza un error 404 en caso de no encontrar datos
	*/
	function renderError() {
	    var error = $('<div>');

	    error.addClass("error");
	    error.append($('<h2>', {text: "Lamentamos no haber encontrado el objeto seleccionado"}));
		error.append($('<p>', {html: 'Pruebe a volver al <a href="index.html">inicio</a> o a realizar otra busqueda'}));
		$('section.results div.resultados #largePagination').empty();
		$('section.results div.resultados #shortPagination').empty();
		$('section.results div.resultados div.error').empty();
		$('section.results div.resultados').append(error);
		setNResultados(0);
	}

	/*
	Genera el array con los resultados
	*/
	function generateResults1(category, subCat) {
		var objs = [];
		//filtramos por categoría: por cada producto de nuestra lista de productos -> agregamos a la lista
		var checkedCat = localStorage.getItem('subCatFiltros');
		var checkedCity = localStorage.getItem('ciudadesFiltros');
		var precios = $('#sliderPrecio').slider("values");
		for (i in products) {
			//si la categoría coincide, se agrega. Si la categoría es 0(todos), tambien se agrega
			if( (products[i]['categoria'].localeCompare(category) == 0) || (category.localeCompare('Todo') == 0) ) {
				//se comprueba que la subcategoría concuerde y su filtro
				if (products[i]['subcategoria'].localeCompare(subCat) == 0) {
					//la primera vez que se entra, los filtros almacenados son undefined
					if ((checkedCat == undefined || checkedCat.indexOf(products[i]['subcategoria']) != -1)
						&& (checkedCity == undefined || checkedCity.indexOf(products[i]['ciudad']) != -1 )
						&& ((products[i]['precio'] >= precios[0] || precios[0] == 0) && products[i]['precio']<= precios[1])) {
						objs.push(products[i]);
					}
				}
			}
		}
		return objs;
	}

	function generateResults2(category, search) {
		var objs = [];
		//filtramos por categoría: por cada producto de nuestra lista de productos -> agregamos a la lista
		var checkedCat = localStorage.getItem('subCatFiltros');
		var checkedCity = localStorage.getItem('ciudadesFiltros');
		var precios = $('#sliderPrecio').slider("values");
		//buscamos por palabras, más que por coincidencia de la frase
		var variablesBusqueda = search.split("+");
		for (i in variablesBusqueda) {
			//quitamos palabras no relevantes (en, el, un...)
			if (variablesBusqueda[i].length < 3) {
				variablesBusqueda.splice(i, i+1);
			}
		}

		for (i in products) {
			//si la categoría coincide, se agrega. Si la categoría es todos, tambien se agrega
			if( (products[i]['categoria'].localeCompare(category) == 0) || (category.localeCompare('Todo') == 0) ) {
				if ( (checkedCat == undefined || checkedCat.indexOf(products[i]['subcategoria']) != -1)
						&& (checkedCity == undefined || checkedCity.indexOf(products[i]['ciudad']) != -1 )
						&& ((products[i]['precio'] >= precios[0] || precios[0] == 0) && products[i]['precio']<= precios[1])) {
					//si la categoría coincide, se comprueban todos los tags con la busqueda
					for (j in products[i]['tags']) {
						var coincidencia = 0;
						for (k in variablesBusqueda) {
							if (products[i]['tags'][j].localeCompare(variablesBusqueda[k]) == 0) {
								objs.push(products[i]);
								coincidencia = 1;
								break;
							}
						}
						if (coincidencia == 1) { break; }
					}
				}
			}
		}

		return objs;
	}

	function generateResults3(category) {
		var objs = [];
		//filtramos por categoría: por cada producto de nuestra lista de productos -> agregamos a la lista
		var checkedCat = localStorage.getItem('subCatFiltros');
		var checkedCity = localStorage.getItem('ciudadesFiltros');
		var precios = $('#sliderPrecio').slider("values");
		for (i in products) {
			//si la categoría coincide, se agrega. Si la categoría es 0(todos), tambien se agrega
			if( (products[i]['categoria'].localeCompare(category) == 0) || (category.localeCompare('Todo') == 0) ) {
				if ((checkedCat == undefined || checkedCat.indexOf(products[i]['subcategoria']) != -1)
						&& (checkedCity == undefined || checkedCity.indexOf(products[i]['ciudad']) != -1 )
						&& ((products[i]['precio'] >= precios[0] || precios[0] == 0) && products[i]['precio']<= precios[1])) {
					//se agrega a la lista
					objs.push(products[i]);
				}
			}
		}

		return objs;
	}

	//Se renderizan los objetos
	function renderResults(objs, view) {
		if (objs.length == 0) {
			renderError();
		} else {
			//filtros
			if (!filtros) { filtros = true; generarFiltros(objs); }
			var conteo = 0;
			var paginaActual;
				for (i in objs) {
					setNResultados(objs.length);
					//actualiza la barra de paginacion
					$('#largePagination').pagination('updateItems',objs.length);
					$('#shortPagination').pagination('updateItems',objs.length);
					//Comprueba el formato de vista y genera los objetos
					if ((view == 'true') || (view == undefined)) {
						localStorage.getItem('largePage')!=undefined ? paginaActual = 5*(localStorage.getItem('largePage')-1) : paginaActual=0;
						if ((conteo < 5) && (+i+paginaActual<objs.length)) {
							createItemLarge(objs[+i+paginaActual]['id'], objs[+i+paginaActual]['imagen'], objs[+i+paginaActual]['descripcion'],
												objs[+i+paginaActual]['nombre'], objs[+i+paginaActual]['precio']);
							conteo++;
						}
					} else {
						localStorage.getItem('shortPage')!=undefined ? paginaActual = 16*(localStorage.getItem('shortPage')-1) : paginaActual=0;
					//Como el formato compacto ocupa menos, pueden generarse muchos más elementos por página
						if ((conteo < 16) && (+i+paginaActual<objs.length)) {
							createItemShort (objs[+i+paginaActual]['id'], objs[i]['imagen'], objs[+i+paginaActual]['precio']);
							conteo++;
						}
					}
				}
		}
	}

	//Los filtros que aparecen, dependen de los objetos
	function generarFiltros(objs) {
		generarCategorias(objs);
		generarCiudades(objs);
		updatePrecios(objs);
	}

	/*<aside class='filtros'>
	<!-- Los filtros con check box-->
	<div id='subcategorias'>
		<span id=categoria>Categoria</span>
		<label><input type="checkbox" value="ej">subcats</label>
	</div>
	Genera las categorias posibles en funcion de los resultados obtenidos */
	function generarCategorias(objs) {
		var container = $('.filtros #subcategorias')[0];
		var subcategorias = [];
		var categorias = [];
		var checkedCat = localStorage.getItem('subCatFiltros');
		for (i in objs) {

			//si la categoría no se encontraba se agrega
			if (categorias.indexOf(objs[i]['categoria']) == -1){
				categorias.push(objs[i]['categoria']);

				var span = document.createElement('span');
				span.innerHTML = objs[i]['categoria'];
				span.setAttribute('id', objs[i]['categoria']);
				container.appendChild(span);
			}

			//si la subcategoría contiene la subcategoría se crea el checkbox en true
			if ( subcategorias.indexOf(objs[i]['subcategoria']) == -1 ) {
				subcategorias.push(objs[i]['subcategoria']);
				var label = document.createElement('label');
					var input = document.createElement('input');
					input.setAttribute('type', 'checkbox');
					input.setAttribute('value', objs[i]['subcategoria']);
					input.setAttribute('checked', true);

				label.appendChild(input);
				label.innerHTML += objs[i]['subcategoria'].charAt(0).toUpperCase() + objs[i]['subcategoria'].slice(1);
				$('#'+objs[i]['categoria'])[0].appendChild(label);
			}
		}

		//se agregan las subcategorias sin checkbox si no se había agregado anteriormente
		for (i in objs) {
			//por cada objeto se comprueban las subcategorias de cada categoría
			for (j in categories[objs[i]['categoria']]) {
				//si la subcategoría no ha sido agregada previamente, se agrega sin checkbox a true
				if ( subcategorias.indexOf(categories[objs[i]['categoria']][j]) == -1) {
					subcategorias.push(categories[objs[i]['categoria']][j]);
					var label = document.createElement('label');
						var input = document.createElement('input');
						input.setAttribute('type', 'checkbox');
						input.setAttribute('value',categories[objs[i]['categoria']][j]);

					label.appendChild(input);
					label.innerHTML += categories[objs[i]['categoria']][j].charAt(0).toUpperCase() + categories[objs[i]['categoria']][j].slice(1);
					$('#'+objs[i]['categoria'])[0].appendChild(label);
				}
			}
		}
	}

	/*
	<div id='ciudades'>
		<span>Ciudades</span>
		<label><input type="checkbox" value="ciudad">Ciudad</label>
	</div>
	Genera las ciudades posibles en funcion de los resultados obtenidos */
	function generarCiudades(objs) {
		var container = $('.filtros #ciudades')[0];
		var ciudades = [];
		var checkedCity = localStorage.getItem('ciudadesFiltros');
		var span = document.createElement('span');
		span.innerHTML = 'Ciudades';
		container.appendChild(span);
		for (i in objs) {
			//si la categoría no se encontraba ya en el filtro, se agrega
			if ( ciudades.indexOf(objs[i]['ciudad']) == -1 ) {
				ciudades.push(objs[i]['ciudad']);
				var label = document.createElement('label');
					var input = document.createElement('input');
					input.setAttribute('type', 'checkbox');
					input.setAttribute('value', objs[i]['ciudad']);
					if (checkedCity != undefined){
						if (checkedCity.indexOf(objs[i]['ciudad']) != -1) {
							input.setAttribute('checked', true);
						}
					} else {
						input.setAttribute('checked', true);
					}
				label.appendChild(input);
				label.innerHTML += objs[i]['ciudad'].charAt(0).toUpperCase() + objs[i]['ciudad'].slice(1);
				span.appendChild(label);
			}
		}
	}


	//Actualiza el slider de precios en función del máximo y mínimo obtenido
	function updatePrecios(objs) {
		var max = 0;
		var min = 0;
		for (i in objs) {
			var aux = objs[i]['precio'];
			if (aux > max) {
				max = aux;
			}
			if (aux < min) {
				min = aux;
			}
		}
		$('#sliderPrecio').slider("option", "max", max);
		$( "#rango" ).val( $( "#sliderPrecio" ).slider( "values", 0 ) + "€ - " + $( "#sliderPrecio" ).slider( "values", 1 ) +"€" );
	}


	function saveFiltros() {
		var subCatFiltros = [];
		$('#subcategorias input:checkbox:checked').each(function() {
			subCatFiltros.push($(this).val());
		});
		var ciudadesFiltros = [];
		$('#ciudades input:checkbox:checked').each(function() {
			ciudadesFiltros.push($(this).val());
		});
		var precios = $('#sliderPrecio').slider("values");

		localStorage.setItem('subCatFiltros', subCatFiltros);
		localStorage.setItem('ciudadesFiltros', ciudadesFiltros);
		localStorage.setItem('sliderPrecio', precio);
	}

	//al poner o quitar un filtro, los resultados varían
	function updateResults() {
		$('#anuncios').empty();
		$('.error').empty();
		saveFiltros();
		searchResults();
	}

	function ordenarResults(objs, ascendente) {
		var auxPrizes = [];
		for (i in objs)
			auxPrizes.push(objs[i]['precio']);

		if (ascendente == undefined || ascendente == 'true')
			auxPrizes.sort(function(a,b){return(a-b)});
		else
			auxPrizes.sort(function(a,b){return(b-a)});

		var ordRes = [];
		for (i in auxPrizes){
			for (j in objs)
				if (auxPrizes[i] == objs[j]['precio']){
					ordRes.push(objs[j]);
					objs.splice(j, 1);
					break;
				}
		}
		return ordRes;
	}

	function imagenesCuadradas () {
		$('div.imagen img').load(function() {
			var alto = $(this).height();
			var ancho = $(this).width();
			var paddVert = 0;
			var paddHor = 0;
			if (ancho > alto)
				paddVert = (150 - ((alto/ancho) * 150))/2;
			if (alto > ancho)
				paddHor = (150 - ((ancho/alto) * 150))/2;
			$(this).css('padding', paddVert + 'px ' + paddHor + 'px');
		});
	}

	});
});
