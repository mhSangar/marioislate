var products = [
	{
		"id": 0,
		"nombre": "Entrenamiento Jedi",
		"descripcion": "Despierta la Fuerza que hay en tí. Aprende a controlar tus poderes y conviertete en lo que siempre quisiste un poderoso caballero Jedi. Antiguo caballero Jedi, con más de 10 años de experiencia.",
		"precio": 100,
		"categoria": "Juegos",
		"subcategoria": "pc",
		"imagen": "images/jedi.jpg",
		"carrousel": ["images/carrousel/jedi1.jpeg","images/carrousel/jedi2.jpeg"],
		"ciudad": "madrid",
		"tags": ["sith", "jedi", "star", "wars", "caballero", "notGeekAtAll"]
	},
	{
		"id": 1,
		"nombre": "Han Solo en carbonita",
		"descripcion": "Se vende Han Solo en carbonita, me he cansado de él, no da conversación y siempre está en la misma posición. Es un aburrido. Se me ha caido un par de veces, pero nada grave.",
		"precio": 600,
		"categoria": "Mascotas",
		"subcategoria": "otros",
		"imagen": "images/han.jpg",
		"carrousel": ["images/carrousel/han1.jpeg",'images/carrousel/han2.jpeg'],
		"ciudad": "albacete",
		"tags": ["han", "solo", "carbonita", "star", "wars", "soloSeQuedoxdddd", "perdonPorElRetraso"]
	},
	{
		"id": 2,
		"nombre": "Dado de 20 caras, versión taza",
		"descripcion": "Taza en perfecto estado, y bastante chula. Sigue en su caja todavía, no ha visto la luz del sol.",
		"precio": 12,
		"categoria": "Moda",
		"subcategoria": "hombre",
		"imagen": "images/d20.jpg",
		"carrousel": [],
		"ciudad": "lepe",
		"tags": ["taza", "dado", "unasMagic?", "mug","d20"]
	},
	{
		"id": 3,
		"nombre": "Enchiridion de bolsillo",
		"descripcion": "Manual de entrenamiento para ser héroe. Lo conseguí hace tiempo y me lo he leído muchas veces para convertirme en Fi... Frank el... Es igual. Algunas páginas se caen, pero ya estaba así cuando lo encontré y a mí me ha servido. Por cierto, pone que es de bolsillo, pero a mí no me cabe. A lo mejor necesitáis bolsillos más grandes.",
		"precio": 70,
		"categoria": "Juegos",
		"subcategoria": "pc",
		"imagen": "images/enchiridion.jpg",
		"carrousel": ['images/carrousel/enchiridion1.jpeg','images/carrousel/enchiridion2.jpeg','images/carrousel/enchiridion3.png'],
		"ciudad": "cuenca",
		"tags": ["enchiridion", "manual", "heroe", "libro", "antiguo", "hora", "aventuras"]
	},
	{
		"id": 4,
		"nombre": "Espada legendaria",
		"descripcion": "¿Quieres ser un héroe inigualable? Pues primero necesitarás una buena espada con la que defenderte. Un amigo mío, que no soy yo, empezó con esta, pero ha conseguido una nueva y no la usa. Solo tiene un par de arañazos.",
		"precio": 20,
		"categoria": "Moda",
		"subcategoria": "hombre",
		"imagen": "images/espada.jpg",
		"carrousel": [],
		"ciudad": "cuenca",
		"tags": ["espada", "poco afilada", "heroe", "libro", "hora", "aventuras", "finn el humano", "digooo... frank"]
	},
	{
		"id": 5,
		"nombre": "Gorro para gatos, diseño Monstruo de las Galletas",
		"descripcion": "Vendo gorros de lana hechos a mano. Mis gatos necesitan más comida de de lata. Por favor, envíen dinero. Oh, Dios mío, se están rebelando... ¡Ayuda! ... ... ... *Fin de la conexión*",
		"precio": 15,
		"categoria": "Mascotas",
		"subcategoria": "gatos",
		"imagen": "images/gorro_gato.jpg",
		"carrousel": [],
		"ciudad": "cuenca",
		"tags": ["gorro", "lana", "gato", "monstruo", "galletas", "...", "dominaremos el mundo"]
	},
	{
		"id": 6,
		"nombre": "Pastilla Roja",
		"descripcion": "Esta pastilla ha servido a muchos humanos a lo largo de la historia para librerarse de los grilletes de una sociedad que les cohibia. Puede ser también tu valvula de escape. Entra en el Pais de las Maravillas y descubre que tan profundo es la madrgiuera del conejo. No dejes que te cuenten la verdad, ven y tomala.",
		"precio": 139,
		"categoria": "Salud",
		"subcategoria": "alimentación",
		"imagen": "images/pastilla-roja.jpg",
		"carrousel": ["images/pastilla-azul.jpg"],
		"ciudad": "madrid",
		"tags": ["pastilla", "roja", "autodescubrimiento", "Trinity", "salvacion"]
	},
	{
		"id": 7,
		"nombre": "Pastilla Azul",
		"descripcion": "Esta pastilla de devolvera a tu placida cama, despertando de un sueño. Será tu decisión creer o no... que demonios, no cojas esta pastilla, coge la roja, ¡por la libertaaaaaad!",
		"precio": 590,
		"categoria": "Salud",
		"subcategoria": "alimentación",
		"imagen": "images/pastilla-azul.jpg",
		"carrousel": ["images/pastilla-roja.jpg"],
		"ciudad": "barcelona",
		"tags": ["pastilla", "azul", "cobarde", "Smith"]
	},
	{
		"id": 8,
		"nombre": "Pip-Boy 3000",
		"descripcion": "Cuando salgas al peligroso Yermo, cuando pasees por sus calidas y radiactivas praderas, ¡el Pip-Boy 3000 es tu mejor compañero! Lucha contra metahumanos, supermutantes, cucarachas gigantes e incluso extraterrestres, siempre acompañado por tu fiel y completo PiP-Boy 3000. ¡Es tan potente que sobrevivirá incluso a tu pulverización!",
		"precio": 1580,
		"categoria": "Smartphones",
		"subcategoria": "accesorios",
		"imagen": "images/Pip-Boy_3000.jpg",
		"carrousel": ['images/carrousel/pipboy1.jpeg','images/carrousel/pipboy2.jpeg'],
		"ciudad": "sevilla",
		"tags": ["pip-boy", "lana", "3000", "Yermo", "caminante", "superate a ti mismo", "radiación"]
	},
	{
		"id": 9,
		"nombre": "Caja de cristal",
		"descripcion": "Vendo la vivienda de mi vida. Ha sido fuente de orgullo y transparencia para mi. Aquí no se cometen delitos, todos está limpio y claro. Mucha luz, ideal para trabajar de cara al público. Gran presencia animal. Los cánticos alegres se compran a parte.",
		"precio": 35000,
		"categoria": "Vivienda",
		"subcategoria": "venta",
		"imagen": "images/caja-cristal.jpg",
		"carrousel": [],
		"ciudad": "alcobendas",
		"tags": ["vivienda", "caja", "cristal", "postmodernista", "Casa", "Disney"]
	},
	{
		"id": 10,
		"nombre": "Cánticos de alegría",
		"descripcion": "Vendo los alegres cánticos de una gran amiga que siempre ocupará un lugar en mi manzana. Atraen tanto a pajaros como a principes azules (los principes también azules). Especiales para animar el hogar. Se complementa muy bien con mi otra venta, la Caja de Cristal",
		"precio": 3000,
		"categoria": "Informatica",
		"subcategoria": "software",
		"imagen": "images/cassette.png",
		"carrousel": [],
		"ciudad": "alcobendas",
		"tags": ["vivienda", "cristal", "postmodernista", "Casa", "Disney"]
	},
	{
		"id": 11,
		"nombre": "Chaqueta para gatetes",
		"descripcion": "Embellece ese gato tan feo que te has comprado con esta moderna chaqueta hecha a mano. 100% Lino, no irrita la piel de los gatos (aunque no es inmune a sus arañazos). La peluca no viene incluida.",
		"precio": 30,
		"categoria": "Mascotas",
		"subcategoria": "gatos",
		"imagen": "images/chaq_gato.jpg",
		"carrousel": ["images/gorro_gato.jpg"],
		"ciudad": "barcelona",
		"tags": ["gato", "chaqueta", "mano", "queGatoMasFeoPorDiosxddd"]
	}

];

var categories = {
	"Motor": ["coches", "motos", "karts"],
	"Informatica": ["componentes", "perifericos", "software"],
	"Moda": ["mujer", "hombre", "infantil"],
	"Mascotas": ["perros", "gatos", "otros"],
	"Smartphones": ["ios", "android", "accesorios"],
	"Juegos": ["pc", "ps4", "one"],
	"Salud": ["alimentación", "medicación", "cosmética"],
	"Vivienda": ["alquiler", "compra", "venta"]
}

var cities = ["madrid", "albacete", "lepe", "barcelona","cuenca","alcobendas","sevilla"];

var owners = [
	{
		"id": 0,
		"nombre": "Juan Palomo",
		"telefono": "660 660 660",
		"email": "Juan@poc.com",
		"items": [0,2]
	},
	{
		"id": 1,
		"nombre": "Han Con Leche",
		"telefono": "770 770 770",
		"email": "HanElGracioso@solo.com",
		"items": [1]
	},
	{
		"id": 2,
		"nombre": "Elena Nito",
		"telefono": "344 567 767",
		"email": "delBosque@blancanieves.com",
		"items": [9,10]
	},
	{
		"id": 3,
		"nombre": "Frank el Humano",
		"telefono": "657 254 895",
		"email": "miNombreEsFinn@Ooo.com",
		"items": [3, 4]
	},
	{
		"id": 4,
		"nombre": "Kit",
		"telefono": "685 422 159",
		"email": "kat_lover@paws.com",
		"items": [5,11]
	},
	{
		"id": 5,
		"nombre": "Agent Smith",
		"telefono": "000 000 000",
		"email": "cazaHumanos@matrix.net",
		"items": [7,6]
	},
	{
		"id": 6,
		"nombre": "Refugio 101",
		"telefono": "101 101 101",
		"email": "losEscondidos@vault.org",
		"items": [8]
	}

];
