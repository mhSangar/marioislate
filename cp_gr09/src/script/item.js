$(document).ready(function () {

    //Comprueba si recibe un id
    var idItem = getUrlVars()['id'];
    if (idItem && (idItem < products.length)) {
        //Genera el contenido de la pagina a partir del id
        generateItem(products[idItem]);
    	generateContact(owners[getOwner(idItem)]);


    	$('div.form form').validate({
    		debug: true,
    		rules: {
    			telephone: {
    				required: true,
    				digits: true
    			}
    		},
    		submitHandler: function(form) {
    			$.notify(form, "¡Contactado!", "success");
                form.reset();
    		}
    	});
    } else {
        renderError();
    }

	$('ul.tags li a').click(function () {
		//limpiamos localStorage para que el breadcrumb sea correcto
		localStorage.clear();
		//Guardamos los nuevos valores de búsqueda
		localStorage.setItem('search', this.innerHTML);
		localStorage.setItem('category', 'Todo');	
	});

});


/*
	Esta función genera un mapa cuya clave es el nombre de las variables pasadas por GET y el valor es el contenido de las mismas
*/
function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		vars[key] = value;
		});
	return vars;
}

/*
	Función que devuelve el id del propietario
*/
function getOwner(data) {
	for (var owner of owners) {
		for (item of owner.items) {
			if (item == data) {
				return owner.id;
			}
		}
	}
}

/*
	Función que genera la pagina de item
*/
function generateItem(data) {
	//breadcrumb
	
	//console.log($('.bc_search button')[0]); 
	$('.bc_search button')[0].setAttribute('value', localStorage.getItem('category'))
	if (localStorage.getItem('subcategory') != null)
		$('.bc_search input[name="subcategory"]')[0].setAttribute('value', localStorage.getItem('subcategory'))
	if (localStorage.getItem('search') != "" && localStorage.getItem('search') != null)
		$('.bc_search input[name="search"]')[0].setAttribute('value', localStorage.getItem('search'))
	var bc_title = $('.breadcrumb #titulo')[0];
	bc_title.innerHTML = data.nombre;
	bc_title.setAttribute('href', window.location.href);

	//img
	$('div.images img')[0].setAttribute("src", data.imagen);
	$('div.images img')[0].setAttribute("alt", "imagen");

	//carrousel
	var carrousel = $('div.images div.carrousel')[0];
	for(var pos in data.carrousel) {
		var carrouselImage = document.createElement('img');
		carrouselImage.setAttribute("src", data.carrousel[pos]);
		carrousel.appendChild(carrouselImage);
	}

	//Cabecera del articulo
	$('div.article div.head h3')[0].innerHTML = data.nombre;
	$('div.article div.head p.price')[0].innerHTML = data.precio + " €";

	//Cuerpo del articulo
	$('div.article div.body p.description')[0].innerHTML = data.descripcion;
	//Colección de tags
	var tags = $('div.article div.body ul.tags')[0];
	for(var tag in data.tags) {
		var liTag = document.createElement('li');
			
			var aTag = document.createElement('a');
			//aTag.setAttribute('href', 'results.html?category=' + data.categoria + '&search=' + data.tags[tag]);
			aTag.setAttribute('href', 'results.html?category=Todo&search=' + data.tags[tag]);
			aTag.innerHTML = data.tags[tag];

		liTag.appendChild(aTag);
		tags.appendChild(liTag);
	}
}

/*
	Función que genera el contacto del item
*/
function generateContact(data) {
	$('div.contact #name p')[0].innerHTML = data.nombre;
	$('div.contact #telephone p')[0].innerHTML = data.telefono;
	$('div.contact #email p')[0].innerHTML = data.email;
}

/*
    Renderiza un error 404 en caso de no encontrar datos
*/
function renderError() {
    var section = $('section')[0];
    var error = document.createElement('div');

    error.setAttribute("class", "error");
    error.appendChild(document.createElement("h2"));
    error.firstChild.innerHTML = "Lamentamos no haber encontrado el objeto seleccionado";
    error.appendChild(document.createElement("p"));
    error.lastChild.innerHTML = 'Pruebe a volver al <a href="index.html">inicio</a> o a realizar otra busqueda';

    while (section.firstChild) {
        section.removeChild(section.firstChild);
    }

    section.appendChild(error);
}
